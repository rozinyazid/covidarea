Ext.define('Covid.store.Climate', {
    extend: 'Ext.data.Store',
    alias: 'store.climate',
    storeId: 'climate',

    fields: [
        'month',
        'high',
        'low',
        {
            name: 'highF',
            calculate: function (data) {
                return data.high * 1.8 + 32;
            }
        },
        {
            name: 'lowF',
            calculate: function (data) {
                return data.low * 1.8 + 32;
            }
        }
    ],
    data: [
        { month: 'kulim', high: 14.7, low: 5.6  },
        { month: 'air dingin', high: 16.5, low: 6.6  },
        { month: 'panam', high: 18.6, low: 7.3  },
      
    ],

    counter: 0,

    generateData: function () {
        var data = this.config.data,
            i, result = [],
            temp = 15,
            min = this.counter % 2 === 1 ? 0 : temp;
        for (i = 0; i < data.length; i++) {
            result.push({
                month: data[i].month,
                high: min + temp + Math.random() * temp,
                low: min + Math.random() * temp
            });
        }
        this.counter++;
        return result;
    },

    refreshData: function () {
        this.setData(this.generateData());
    }

});