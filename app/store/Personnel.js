Ext.define('Covid.store.Personnel', {
    extend: 'Ext.data.Store',
    storeId: 'personnel',
    alias: 'store.personnel',

    fields: [
        'name', 'email', 'phone','add'
    ],

    data: { items: [
        { photo:'<img src="resources/1.jpg" alt="" width="100px" height="75">' ,name: 'RS.Awal Bros', email: "awalbross@enterprise.com", phone: "555-111-1111" ,add: "Panam pekanbaru", tipe:"RSS" },
        { photo:'<img src="resources/2.jpg" alt="" width="100px" height="75">' ,name: 'RSU Pekanbaru',     email: "umum@enterprise.com",  phone: "555-222-2222",add: "Arifin achmad pekanbaru" , tipe:"RSU" },
        { photo:'<img src="resources/3.jpg" alt="" width="100px" height="75">' ,name: 'RS. Aulia',   email: "aulia@enterprise.com",    phone: "555-333-3333" ,add: "Panam pekanbaru" , tipe:"RSS"},
        { photo:'<img src="resources/4.jpg" alt="" width="100px" height="75">' ,name: 'RS. Syafira',     email: "syafira@enterprise.com",        phone: "555-444-4444" ,add: "Sudirman pekanbaru" , tipe:"RSS"}
    ]},

    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});
