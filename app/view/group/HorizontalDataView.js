Ext.define('Covid.view.group.HorizontalDataView', {
    extend: 'Ext.Container',
    xtype: 'bdataview',
    // id: 'bdataview',
    requires: [
        // 'KitchenSink.model.Speaker',
        'Ext.dataview.plugin.ItemTip',
        'Covid.store.Personnel',
        'Ext.plugin.Responsive',
        'Ext.field.Search'
    ],

        viewModel:{
        stores:{
            personnel:{
                type:'personnel',
            }
        }
    },

       layout: 'fit',
    cls: 'ks-basic demo-solid-background',
    shadow: true,
    items: [{
            docked: 'top',
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'searchfield',
                    placeHolder: 'Search',
                    name: 'searchfield',
                    listeners:{
                        change: function(me,newValue , oldValue,eOpts){
                         alert(newValue) ;  
                         personnelStore= Ext.getStore('personnel');
                        // personnelStore.filter('name',newValue); //pencarian nama
                        personnelStore.filter('name',newValue); // 
                        // personnelStore.filter('email',newValue);
                        }
                    }
                }
            ]
        },{
        xtype: 'dataview',
        scrollable: 'y',
        cls: 'dataview-basic',
        inline: {
            wrap: false
        },
        itemTpl: '{photo}<br><font size= 4 color="red">{nama}</font><br><b>{email}</b><br><i>{phone}</i>-<u><br>{add}</u><hr><br>',
        bind: {
            store: '{personnel}'
        },
        plugins: {
            type: 'dataviewtip',
            align: 'l-r?',
            plugins: 'responsive',

            responsiveConfig: {
                "width < 600": {
                    align: 'tl-bl?'
                }
            },
            width: 415,
            minWidth: 400,
           
            // delegate: '.img',
            allowOver: true,
            
            anchor: true,
            bind: '{record}',
            tpl: '<table style="border-spacing:3px;border-collapse:separate">' + 
                    '<tr><td></td><td>{photo}</td></tr>' +
                    '<tr><td>Nama: </td><td>{name}</td></tr>' +
                    '<tr><td>Email:</td><td>{email}</td></tr>' + 
                    '<tr><td>Phone:</td><td>{phone}</td></tr>' +
                    '<tr><td>Alamat:</td><td>{alamat}</td></tr>' 
                   
        }
    }]
});