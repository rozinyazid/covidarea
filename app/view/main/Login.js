Ext.define('Covid.view.form.Login', {
    extend: 'Ext.form.Panel',

    requires: [
        'Ext.form.FieldSet',
        'Ext.field.Password'
    ],
    
    shadow: true,
    cls: 'demo-solid-background',
    xtype: 'login', //add
    // id: 'basicform', 
    id: 'login', //apa itu xtype, itu adalah idnya
    controller: 'login',
    items: [
        {
            xtype: 'fieldset',
            id: 'fieldsetlogin',
            title: 'Login Admin',
            instructions: 'Please Login as Admin.',
            defaults: {
                labelWidth: '35%'
            },
            items: [
                {
                    xtype: 'textfield',
                    name: 'username',
                    label: 'Username',
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'passwordfield',
                    revealable: true,
                    name : 'password',
                    label: 'Password',
                    clearIcon: true
                },
            ]
        },
        
        {
            xtype: 'container',
            defaults: {
                xtype: 'button',
                style: 'margin: 1em',
                flex: 1
            },
            layout: {
                type: 'hbox'
            },
            items: [
                {
                    text: 'Login',
                    ui: 'action',
                    // scope: this,
                    hasDisabled: false,
                    handler:'onLogin' 
                }
                
            ]
        }
    ]
});