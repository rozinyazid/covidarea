/**
 * This view is an example list of people.
 */
Ext.define('Covid.view.main.List', {
    extend: 'Ext.grid.Grid',
    xtype: 'mainlist',

    requires: [
        'Covid.store.Personnel'
    ],

    title: 'Rumah sakit yang melayani pasien Covid19',

    store: {
        type: 'personnel'
    },

    columns: [
        { text: 'Nama Rumah Sakit',  dataIndex: 'name', width: 200 },
        { text: 'Email', dataIndex: 'email', width: 230 },
        { text: 'Phone', dataIndex: 'phone', width: 150 },
        { text: 'Alamat', dataIndex: 'add', width: 150 }

    ],

    listeners: {
        select: 'onItemSelected'
    }
});
