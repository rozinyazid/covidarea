/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 *
 * TODO - Replace the content of this view to suit the needs of your application.
 */
Ext.define('Covid.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',
    id: 'app-main',

    requires: [
        'Ext.MessageBox',

        'Covid.view.main.MainController',
        'Covid.view.main.MainModel',
        'Covid.view.main.List',
        'Covid.view.regis.Regis',
        'Covid.view.group.HorizontalDataView',
        'Covid.view.Setting.Tabs',
        'Covid.view.form.Login',
        'Covid.view.tree.TreePanel', //3
        'Covid.view.chart.Column'

    ],

    controller: 'main',
    viewModel: 'main',

    defaults: {
        tab: {
            iconAlign: 'top'
        },
        styleHtmlContent: true
    },

    tabBarPosition: 'bottom',

    items: [
        {
            title: 'Home',
            iconCls: 'x-fa fa-home',
            layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items: [{
                xtype: 'mainlist'
            }]
        },{
            title: 'Regis Pasien',
            iconCls: 'x-fa fa-user',
            items: [{
                xtype: 'regis'
            }]
        },{
            title: 'List RumahSakit',
            iconCls: 'x-fa fa-users',
            layout: 'fit',
            items: [{
                xtype: 'bdataview'
            }]
        },{
            title: 'Saran',
            iconCls: 'x-fa fa-cog',
            items: [{
                xtype: 'userform'
            }]
        },{
            title: 'Chart',
            iconCls: 'x-fa fa-cog',
            layout: 'fit',
            items: [{
                xtype:'column-chart'
            }]
        }
    ]
});
