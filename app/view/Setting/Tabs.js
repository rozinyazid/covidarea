Ext.define('Covid.view.Setting.Tabs', {
    extend: 'Ext.form.Panel',
    xtype: 'userform',

    requires: [
        'Ext.form.FieldSet',
        'Ext.field.Text',
        'Ext.field.TextArea'
    ],

    shadow: true,
    cls: 'demo-solid-background',
    items: {
        xtype: 'userform',
        defaults: {
            labelAlign: 'top'
        },
        items: [
            {
                xtype: 'toolbar',
                docked: 'bottom',
                scrollable: {
                    y: false
                 },
                 items: [ {
                    xtype: 'button',
                    text: 'Send',
                    ui: 'action',
                    handler: function(){
                    Ext.Msg.alert('alert','Terimakasih atas saran yang diberikan');
                    }
                },
                {
                    xtype: 'spacer'
                },
                    {
                    text: 'Back to home',
                    ui: 'action',
                    handler: function(){
                        alert('kita pindah ke home');
                       var mainView = Ext.getCmp('app-main');
                       mainView.setActiveItem(0);

                    }
                    
                }]
            },
            {
                xtype: 'textfield',
                label: 'Nama',
                labelAlign: 'placeholder'
            },
            {
                xtype: 'textfield',
                label: 'Email',
                labelAlign: 'placeholder'
            },
            {
                xtype: 'textfield',
                label: 'No-hp',
                labelAlign: 'placeholder'
            },
            {
                xtype: 'textareafield',
                label: 'Alamat',
                labelAlign: 'placeholder'
            },

            {
                xtype: 'textareafield',
                label: 'Kritik dan saran',
                labelAlign: 'placeholder'
            }
        ]
    }
});