Ext.define('Covid.view.regis.Regis', {
    extend: 'Ext.form.Panel',

    requires: [
        'Ext.form.FieldSet',
        'Ext.field.Number',
        'Ext.field.Spinner',
        'Ext.field.Password',
        'Ext.field.Email',
        'Ext.field.Url',
        'Ext.field.DatePicker',
        'Ext.field.Select',
        'Ext.field.Hidden',
        'Ext.field.Radio'
    ],
    shadow: true,
    cls: 'demo-solid-background',
    id: 'regis',
    xtype: 'regis',
    items: [
        {
            xtype: 'fieldset',
            id: 'fieldset1',
            title: 'Personal Info',
            instructions: 'Please enter the information above.',
            defaults: {
                labelWidth: '35%'
            },
            items: [
                {
                    xtype: 'textfield',
                    name: 'name',
                    label: 'Nama Pasien',
                    placeHolder: 'Masukkan nama pasien',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },

                 {
                    xtype: 'textfield',
                    name: 'address',
                    label: 'Alamat Pasien',
                    placeHolder: 'Masukkan alamat pasien',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },

                // {
                //     xtype: 'passwordfield',
                //     revealable: true,
                //     name : 'password',
                //     label: 'Password',
                //     clearIcon: true
                // },
                {
                    xtype: 'emailfield',
                    name: 'email',
                    label: 'Email (opsional)',
                    placeHolder: 'me@sencha.com',
                    clearIcon: true
                },
                // {
                //     xtype: 'urlfield',
                //     name: 'url',
                //     label: 'Url',
                //     placeHolder: 'http://sencha.com',
                //     clearIcon: true
                // },
                // {
                //     xtype: 'spinnerfield',
                //     name: 'spinner',
                //     label: 'Spinner',
                //     minValue: 0,
                //     maxValue: 10,
                //     clearable: true,
                //     stepValue: 1,
                //     cycle: true
                // },
                {
                    xtype: 'checkboxfield',
                    name: 'cool',
                    label: 'Cool',
                    platformConfig: {
                        '!desktop': {
                            bodyAlign: 'end'
                        }
                    }
                },
                {
                    xtype: 'datepickerfield',
                    destroyPickerOnHide: true,
                    name: 'date',
                    label: 'Tanggal masuk',
                    value: new Date(),
                    picker: {
                        yearFrom: 1990
                    }
                },
                {
                    xtype: 'selectfield',
                    name: 'sex',
                    label: 'Jenis Kelamin',
                    options: [
                        
                        {
                            text: 'Perempuan',
                            value: 'perempuan'
                        },
                        {
                            text: 'Laki-laki',
                            value: 'Laki-laki'
                        }
                    ]
                },
                // {
                //     xtype: 'sliderfield',
                //     name: 'slider',
                //     label: 'Slider'
                // },
                {
                    xtype: 'togglefield',
                    name: 'toggle',
                    label: 'Swab test(tandai jika sudah mengisi)'
                },
                {
                    xtype: 'textareafield',
                    name: 'bio',
                    label: 'Bio'
                }
            ]
        },
        // {
        //     xtype: 'fieldset',
        //     id: 'fieldset2',
        //     title: 'Favorite color',
        //     platformConfig: {
        //         '!desktop': {
        //             defaults: {
        //                 bodyAlign: 'end'
        //             }
        //         }
        //     },
        //     defaults: {
        //         xtype: 'radiofield',
        //         labelWidth: '35%'
        //     },
        //     items: [
        //         {
        //             name: 'color',
        //             value: 'red',
        //             label: 'Red'
        //         },
        //         {
        //             name: 'color',
        //             label: 'Blue',
        //             value: 'blue'
        //         },
        //         {
        //             name: 'color',
        //             label: 'Green',
        //             value: 'green'
        //         },
        //         {
        //             name: 'color',
        //             label: 'Purple',
        //             value: 'purple'
        //         }
        //     ]
        // },
        {
            xtype: 'container',
            defaults: {
                xtype: 'button',
                style: 'margin: 1em',
                flex: 1
            },
            layout: {
                type: 'hbox'
            },
            items: [
                {
                    text: 'Disable fields',
                    ui: 'action',
                    scope: this,
                    hasDisabled: false,
                    handler: function(btn){
                        var fieldset1 = Ext.getCmp('fieldset1'),
                            fieldset2 = Ext.getCmp('fieldset2');

                        if (btn.hasDisabled) {
                            fieldset1.enable();
                            fieldset2.enable();
                            btn.hasDisabled = false;
                            btn.setText('Disable fields');
                        } else {
                            fieldset1.disable();
                            fieldset2.disable();
                            btn.hasDisabled = true;
                            btn.setText('Enable fields');
                        }
                    }
                },
                {
                    text: 'Reset',
                    ui: 'action',
                    handler: function(){
                        Ext.getCmp('basicform').reset();
                    }
                },
                {
                    text: 'Back to Home',
                    ui: 'action',
                    handler: function(){
                        alert('Kita akan ke menu Home');
                        var mainView= Ext.getCmp('app-main');
                        mainView.setActiveItem(0);
                    }
                }
            ]
        }
    ]
});