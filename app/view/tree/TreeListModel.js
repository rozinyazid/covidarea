Ext.define('Covid.view.tree.TreeListModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.tree-list',

    formulas: {
        selectionText: function(get) {
            var selection = get('treelist.selection'),
                path;
            if (selection) {
                path = selection.getPath('text');
                path = path.replace(/^\/Root/, '');
                return 'Selected: ' + path;
            } else {
                return 'No node selected';
            }
        }
    }, 

    stores: {
        navItems: {
            type: 'tree',
            rootVisible: true,
            root: {
                expanded: true,
                text: 'All',
                iconCls: 'x-fa fa-sitemap',
                children: [{
                        text: 'RSS',
                        iconCls: 'x-fa fa-home',
                        leaf: true
                    },{
                        
                        text: 'RSU',
                        iconCls: 'x-fa fa-database',
                        leaf: true
                }]
            }
        }
    }
});