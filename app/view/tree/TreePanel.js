Ext.define('Covid.view.tree.TreePanel', {
    extend: 'Ext.Container',
    xtype:'tree-panel',

    requires: [
        'Ext.layout.HBox',
        'Covid.view.tree.Treelist'
    ],

    layout: {
        type: 'hbox',
        pack: 'center',
        align: 'stretch'
    },
    margin: '0 10',
    defaults: {
        margin: '0 0 10 0',
        bodyPadding: 10
    },
    items: [
        {
            xtype: 'tree-list',
            flex:1
        },
        {
            xtype: 'panel',
            id:'detailtree',
            // html:'belum ada terpilih',
            xtype: 'bdataview',
            flex:1
        }
    ]
});