Ext.define('Covid.view.tree.Treelist', {
    extend: 'Ext.grid.Tree', 
    xtype: 'tree-list',
    requires: [
        'Ext.grid.plugin.MultiSelection'
    ],

    
    cls: 'demo-solid-background',
    shadow: true,

    viewModel: {
        type: 'tree-list'
    },

    bind: '{navItems}', //binding ke store yg ada dalam model

    listeners:{
        itemtap: function ( me, index, target, record, e, eOpts ) 
            {
                // console.log (record);
                // detailtree= Ext.getCmp('detailtree');
                
                // detailtree.setHtml("Anda memilih " +record.data.text);
                // alert("Anda memilih " +record.data.text);
                detailtree= Ext.getStore('personnel');
                detailtree.filter('tipe', record.data.text);
            }
    }
});